
<!DOCTYPE html>
<html>
  <head>
    <title>Construtores e Destrutores</title>
    <meta content="utf-8">
    <style></style>
  </head>
  <body>
    <h1 style="background-color: gray">Aprendendo PHP - Construtores e Destrutores</h1>
    
    <?php echo "<h2>Hello World!</h2>"; ?>
    
    <?php 
        class MyDestructableClass {
            function __construct() {
                print "In constructor\n";
                $this->name = "MyDestructableClass";
            }

            function __destruct() {
                print "<br> Destroying " . $this->name . "\n";
            }
        }

        $obj = new MyDestructableClass();
        echo "Friendly!";
    ?>
    
  </body>
</html>
