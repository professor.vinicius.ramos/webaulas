<?php

echo "Hello, MVC em aula!!!<br /><br /><hr /><br /><br />";

/**
 * Front Controller
 * Author: Vinicius Ramos
 */
//Passo 1
//echo 'Requisição URL: "'. $_SERVER['QUERY_STRING'] . '"';

//Passo 2
/*require '../Core/Router.php';

$router = new Router();

echo get_class($router);

*/
//Passo 3

require '../Core/Router.php';

$router = new Router();
// Adicionar as rotas
$router->add('', ['controller'=> 'Home', 'action' => 'index']);
$router->add('produtos', ['controller'=> 'Produtos', 'action' => 'index']);
$router->add('produtos/novo', ['controller'=> 'Produtos', 'action' => 'new']);

// Mostrar a tabela de roteamento

echo "<pre>";
var_dump($router->getRoutes());
echo "</pre>";

// Passo 4

//Repetir parte do Passo 3. Retirar apenas o var_dump()
/*
// Match a rota requerida
$url = $_SERVER['QUERY_STRING'];

if ($router->match($url)) {
  echo "<pre>";
  var_dump($router->getParams());
  echo "</pre>";
} else {
  echo "Rota não encontrada para a URL: '$url'";
}
*/
