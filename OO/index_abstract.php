<?php

require "ClasseAbstrata.php";
require "ClasseConcreta1.php";
require "ClasseConcreta2.php";

$classe1 = new ClasseConcreta1();
$classe1->imprimir();
echo "<br />";
echo "Valor com Prefixo: " . $classe1->valorComPrefixo('FOO_') ."<br />";

$classe2 = new ClasseConcreta2();
$classe2->imprimir();
echo $classe2->valorComPrefixo('FOO_') ."\n";
