<?php

class Conexao
{

  private static $host = "localhost";
  private static $usuario = "root";
  private static $senha = "root";
  private static $base_de_dados = "aulasweb";

  public static function getConn() {
    $conn = mysqli_connect(Conexao::$host, Conexao::$usuario, Conexao::$senha, Conexao::$base_de_dados);

    if(!$conn) {
      echo "Error: Unable to connect to MySQL." . PHP_EOL;
      echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
      echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
      exit;
    }
    return $conn;
  }

}
