<?php
session_start();

require_once "conexao_pdo.php";

$conexao = new Conn();
$conn = $conexao->getConn();

$dados = filter_input_array(INPUT_POST);
//var_dump($dados);

$username = $dados['nomeUsuario'];
$email = $dados['email'];
$senha = $dados['senha'];

$sql = "insert into usuarios (username, email, senha) values (:usuario, :e_mail, :pass)";
$exec = $conn->prepare($sql);
$exec->bindParam(":usuario", $username, PDO::PARAM_STR);
$exec->bindParam(":e_mail", $email, PDO::PARAM_STR);
$exec->bindParam(":pass", $senha, PDO::PARAM_STR);

if ($exec->execute()) {
  $mensagem = "<div class='container'><div class='alert alert-success alert-dismissible fade show' role='alert'>". $username . " cadastrado com sucesso!";
  $mensagem .= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
                  </button></div></div>";
  $_SESSION['mensagem'] = $mensagem;

} else {
  $mensagem = "<div class='container'><div class='alert alert-danger alert-dismissible fade show' role='alert'>ERROR: ". $username . " NÃO cadastrado.";
  $mensagem .= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
                  </button></div></div>";
  $_SESSION['mensagem'] = $mensagem;
}
header("Location: cadastro_usuario.php");
