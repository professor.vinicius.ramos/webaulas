<?php

require_once "pessoa.php";

class Funcionario implements Pessoa
{
  private $nome;
  private $salario;


  public function __construct($n, $s = 0)
  {
    $this->nome = $n;
    $this->salario = $s;
  }

  public function falar()
  {
    return "Oi, Mundo!";
  }

  public function chorar()
  {
    return "Buá!!!!!!";
  }

  public function getNome()
  {
    return $this->nome;
  }

  public function setNome($n)
  {
    $this->nome = $n;
  }

  public function getSalario()
  {
    return $this->salario;
  }

  public function setSalario($s)
  {
    $this->salario = $s;
  }

  public function toString()
  {
    return "Nome: " . $this->nome . " Salário: " . $this->salario;
  }

}
