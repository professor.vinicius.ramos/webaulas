<?php

require_once "funcionario.php";

define("CONS", 100);

class Gerente extends Funcionario
{


  private $caixa = 0;

  public function fazerSangria($valor)
  {
    return ($this->caixa + $valor);
  }

  public function atualizarCaixa($v)
  {
    $this->caixa = $v;
  }

}
