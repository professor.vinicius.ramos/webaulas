<?php session_start(); ?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Signin Template · Bootstrap</title>



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap.css">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <div class="container col-md-12">
<?php
if(isset($_SESSION['mensagem'])) {
  echo $_SESSION['mensagem'];
  unset($_SESSION['mensagem']);
}

if(!isset($_SESSION['logado']) || !$_SESSION['logado']) {

 ?>

      <form class="form-signin" method="post" action="act_cadastro_usuario.php">
        <h1 class="h3 mb-3 font-weight-normal">Cadastrar Usuário</h1>
        <label for="inputNomeUsuario" class="sr-only">Nome de Usuário</label>
        <input type="text" name="nomeUsuario" id="inputNomeUsuario" class="form-control" placeholder="Nome de usuário" required autofocus>
        <label for="inputEmail" class="sr-only">eMail</label>
        <input type="text" name="email" id="inputEmail" class="form-control" placeholder="email@email.com" required>
        <label for="inputSenha" class="sr-only">Senha</label>
        <input type="password" name="senha" id="inputSenha" class="form-control" placeholder="Senha" required>
        <input class="btn btn-lg btn-dark btn-block" name="cadastrar_usuario" type="submit" value="Cadastrar">
        <p class="mt-5 mb-3 text-muted">&copy; 2017-2019</p>
      </form>

<?php
} else {
  ?>
  <form class="form-signin" action="logout.php" method="post">
    <button class="btn btn-lg btn-dark btn-block" name="logout" type="submit">logout</button>
  </form>
<?php
}

?>

<?php

//Conexão ao BD
#require "conexao.php"

$host = "localhost";
$usuario = "root";
$senha = "root";
$base_de_dados = "aulasweb";

$conn = mysqli_connect($host, $usuario, $senha, $base_de_dados);

if(!$conn) {
  echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

//Recuperar os valores do formulário
if (isset($_POST['nomeUsuario']) && isset($_POST['senha'])) {
  $usuario = $_POST['nomeUsuario'];
  $senha = $_POST['senha'];

  //Verificar no BD se o Nome de Usuário e senha existem e estão corretos
  $query = "SELECT * FROM usuarios WHERE senha='$senha' and username='$usuario'";
  $result = mysqli_query($conn, $query);
  if (!$result) {
    echo "Erro do banco de dados, não foi possível consultar o banco de dados\n";
    echo 'Erro MySQL: ' . mysql_error();
    exit;
  }

  if($row = mysqli_fetch_assoc($result)){
    //Se existirem, criar uma sessão.
    $_SESSION['logado'] = true;
    $_SESSION['nomeUsuario'] = $usuario;
  } else {
    echo "<div class='container-fluid alert-danger text-center'>";
    echo "Usuário e senha não encontrados!";
    echo "</div>";
  }

}



?>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="bootstrap-4/js/jquery.min.js"></script>
<script src="bootstrap-4/js/popper.min.js"></script>
<script src="bootstrap-4/js/bootstrap.min.js"></script>

</body>
</html>
