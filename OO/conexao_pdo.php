<?php


class Conn {

  private static $host = "localhost";
  private static $bd_nome = "aulasweb";
  private static $usuario = "root";
  private static $senha = "root";

  public function getConn(){
    $conn = new PDO('mysql:host=' .self::$host. ';dbname=' .self::$bd_nome, self::$usuario, self::$senha);
    return $conn;
  }

}
