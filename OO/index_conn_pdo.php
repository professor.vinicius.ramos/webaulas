<?php

require_once "conexao_pdo.php";

$conexao = new Conn();
$conn = $conexao->getConn();

$dados = filter_input_array(INPUT_POST);
//var_dump($dados);

$username = $dados['nomeUsuario'];
$email = $dados['email'];
$senha = $dados['senha'];

$sql = "insert into usuarios (username, email, senha) values (:usuario, :e_mail, :pass)";
$exec = $conn->prepare($sql);
$exec->bindParam(":usuario", $username, PDO::PARAM_STR);
$exec->bindParam(":e_mail", $email, PDO::PARAM_STR);
$exec->bindParam(":pass", $senha, PDO::PARAM_STR);

if ($exec->execute()) {
  echo $username . " cadastrado com sucesso!";
} else {
  echo "Error!";
}
