<?php

class ClasseConcreta1 extends ClasseAbstrata
{
  protected function pegarValor() {
    return "ClasseConcreta1";
  }

  public function valorComPrefixo( $prefixo ) {
    return "{$prefixo}ClasseConcreta1";
  }

}
